// LIFO
/*
  last in, first out
  como se fosse um empilhamento de travesseiros, tu nao tira o do meio
*/

// de novo falando de ponteiro
/*
  & simbolo de referencia
  * simbolo de ponteiro
*/

#include <iostream>
#include <stdlib.h>
#include <string>

#define TAM 10

using namespace std;

//
void imprime_vetor(int vetor[TAM]) {
  int cont;

  for(cont = 0; cont < TAM; cont++) {
    cout << vetor[cont] << " - ";
  }
}

//
bool pilha_vazia(int topo) {
  if(topo == -1) return true;
  else return false;
}

//
bool pilha_cheia(int topo) {
  if(topo == TAM-1) return true;
  else return false;
}

// inserir valor no topo da pilha
void pilha_push(int pilha[TAM], int valor, int *topo) {
  // caso pilha lotada (não pode colocar mais valores)
  printf("\n");
  if(pilha_cheia(*topo)) printf("pilha está cheia");
  else {
    *topo = *topo + 1;
    pilha[*topo] = valor;
  }
  imprime_vetor(pilha);
}

// tirar um elemento da pilha
void pilha_pop(int pilha[TAM], int *topo) {
  printf("\n");

  if(pilha_vazia(*topo)) printf("pilha está vazia");
  else {
    printf("\nValor removido: %i", pilha[*topo]);
    pilha[*topo] = 0;
    *topo = *topo - 1;
  }
  printf("\n");
  imprime_vetor(pilha);
}

// criar pilha, com todos os campos = 0
void pilha_construtor(int pilha[TAM], int *topo) {
  *topo = -1; // colocar todo negativo para indicar uma pilha vazia
  for(int i = 0; i < TAM; i++) pilha[i] = 0;
}

int pilha_tamanho(int topo) {
  return topo+1;
}

// mostrar o ultimo valor da pilha
int pilha_get(int pilha[TAM], int *topo) {
  if(pilha_vazia(*topo)) {
    printf("A pilha está vazia;\n");
    return 0;
  }
  else {
    return pilha[*topo];
  }
}

int main() {
  int pilha[TAM];
  int topo = -1; // topo da pilha

  pilha_construtor(pilha, &topo);

  imprime_vetor(pilha);

  pilha_push(pilha, 1, &topo);
  pilha_push(pilha, 2, &topo);
  pilha_push(pilha, 3, &topo);
  pilha_push(pilha, 4, &topo);
  pilha_push(pilha, 5, &topo);

  printf("Ultimo valor da pilha: %i\n", pilha_get(pilha, &topo));

  pilha_push(pilha, 6, &topo);
  pilha_push(pilha, 7, &topo);
  pilha_push(pilha, 8, &topo);
  pilha_push(pilha, 9, &topo);
  pilha_push(pilha, 10, &topo);

  printf("Tamanho da pilha %i", pilha_tamanho(topo));

  pilha_pop(pilha, &topo);
  pilha_pop(pilha, &topo);

}
