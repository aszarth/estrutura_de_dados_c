// busca que parte do meio (pivo) e vai estreitando pra um lado ou outro até chegar no valor desejado
// só funciona em buscas já ordenadas em ordem crescente
#include <iostream>
#include <new>
#include <string>
#include <stdlib.h>
#include <stdio.h> // usar o printf do C normal

#define MAX_TAM 10

using namespace std;

int busca_binaria(int vetor[MAX_TAM], int valorProcurado, int *posicaoEncontrada) {
  int limite_esquerda = 0;
  int limite_direita = MAX_TAM - 1;
  int meio; // pivo

  int tentativa = 0;
  while(limite_esquerda <= limite_direita) {
    meio = (limite_esquerda + limite_direita) / 2;
    // printar de forma demostrativa como funciona os cortes da busca binaria
    tentativa++;
    printf("\n\nTentativa: %i\n", tentativa);
    printf("\nPOS M: [%i], M: [%i]", meio, vetor[meio]);
    printf("\nLPS E: [%i], E: [%i]", limite_esquerda, vetor[limite_esquerda]);
    printf("\nLPS D: [%i], D: [%i]", limite_direita, vetor[limite_direita-1]);
    // valor meio encontrado
    if(valorProcurado == vetor[meio]) {
      *posicaoEncontrada = meio;
      return 1;
    }
    // ajustando os limites laterais
    else if(vetor[meio] < valorProcurado) {
      limite_esquerda = meio + 1;
    }
    else if(vetor[meio] > valorProcurado) {
      limite_direita = meio - 1;
    }
  }
  return -1;
}

void imprime_vetor(int vetor[MAX_TAM]) {
  int i;
  for(i = 0; i < MAX_TAM-1; i++) {
    cout << vetor[i] << " - ";
  }
}




int main() {
  int vetor[MAX_TAM] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  int valorProcurado;
  int posicao, posicaoEncontrada;

  imprime_vetor(vetor);

  printf("Qual numero deseja encontrar?");
  scanf("%d", &valorProcurado);

  if(busca_binaria(vetor, valorProcurado, &posicaoEncontrada) == 1) {
    cout << "\nO valor foi encontrado na posicao: " << posicaoEncontrada;
  } else {
    cout << "Valor nao encontrado";
  }


  return 0;
}
