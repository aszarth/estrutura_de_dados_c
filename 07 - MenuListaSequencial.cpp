#include <iostream>
#include <new> // malok C
#include <string> // string sem ser vetores de caracteres e alocar memoria toda hr
#include <stdlib.h>

using namespace std; // pra usar cout

// lista sequencial
struct pessoa {
  string nome;
  int rg;
};

void imprime_sequencial(pessoa *ponteiroSequencial, int tamanhoDaLista) {
  for(int i = 0; i < tamanhoDaLista; i++) {
    cout << ponteiroSequencial[i].nome << "," << ponteiroSequencial[i].rg << "\n";
  }
}

// *& ponteiro com escopo global
void adc_comeco_sequencial(pessoa *&ponteiroSequencial, int *tamanhoDaLista, string nome, int rg) {
  if(*tamanhoDaLista == 0) { // se a lista for vazia, cria uma nova
    // nova lista sequencial, tam 1
    pessoa *novaListaSequencial = new pessoa[1];
    // add os dados
    novaListaSequencial[0].nome = nome;
    novaListaSequencial[0].rg = rg;
    // atualiza o ponteiro pra nova lista
    ponteiroSequencial = novaListaSequencial;
  }
  else { // IF LISTA JA EXISTE (caso a lista já tenha membros)
    // 1 criar um vetor com uma posição a mais que o normal
    pessoa *novaListaSequencial = new pessoa[*tamanhoDaLista+1];
    // 2 insere o primeiro novo elemento, nessa nova lista, no inicio
    novaListaSequencial[0].nome = nome;
    novaListaSequencial[0].rg = rg;
    // 3 pegar os valores do vetor antigo e colocar no novo depois da nova primeira posição
    for(int i = 0; i < *tamanhoDaLista; i++) {
      novaListaSequencial[i+1].nome = ponteiroSequencial[i].nome;
      novaListaSequencial[i+1].rg = ponteiroSequencial[i].rg;
    }
    // atualiza o ponteiro para a lista nova
    ponteiroSequencial = novaListaSequencial;
  }
  // aumenta o tamanho da lista
  *tamanhoDaLista = *tamanhoDaLista + 1;
}

void adc_fim_sequencial(pessoa *&ponteiroSequencial, int *tamanhoDaLista, string nome, int rg) {
  // 1 cria um vetor maior
  // cria uma lista com um tamanho maior
  pessoa *novaListaSequencial = new pessoa[*tamanhoDaLista+1];
  // 2 inicia colocando os antigos
  for(int i = 0; i < *tamanhoDaLista; i++) {
    novaListaSequencial[i].nome = ponteiroSequencial[i].nome;
    novaListaSequencial[i].rg = ponteiroSequencial[i].rg;
  }
  // 3 por ultimo um novo valor
  // posiciona o ultimo elemento
  novaListaSequencial[*tamanhoDaLista].nome = nome;
  novaListaSequencial[*tamanhoDaLista].rg = rg;

  // atualiza o ponteiro para a lista nova
  ponteiroSequencial = novaListaSequencial;
  // aumenta o tamanho da lista
  *tamanhoDaLista = *tamanhoDaLista + 1;
}

// inserir no meio
void adc_pos_sequencial(pessoa *&ponteiroSequencial, int *tamanhoDaLista, string nome, int rg, int posicao) {
  // 1 criar um vetor maior
  pessoa *novaListaSequencial = new pessoa[*tamanhoDaLista+1];
  // 2 iniciar colocando os valores antigos
  for(int i = 0; i < posicao; i++) {
    novaListaSequencial[i].nome = ponteiroSequencial[i].nome;
    novaListaSequencial[i].rg = ponteiroSequencial[i].rg;
  }
  // 3 qnd chegar na pos X colocar um novo valor
  novaListaSequencial[posicao].nome = nome;
  novaListaSequencial[posicao].rg = rg;
  // 4 retoma a posição dos antigos (-1 do vetor antigo pq o novo tem 1 pos a mais do POS)
  for(int i = posicao+1; i < *tamanhoDaLista+1; i++) {
    novaListaSequencial[i].nome = ponteiroSequencial[i-1].nome;
    novaListaSequencial[i].rg = ponteiroSequencial[i-1].rg;
  }
  // atualizao ponteiro para apontar para a nova lista
  ponteiroSequencial = novaListaSequencial;
  //  aumenta o tamanho da lista
  *tamanhoDaLista = *tamanhoDaLista + 1;
}

void remove_inicio_sequencial(pessoa *&ponteiroSequencial, int *tamanhoDaLista) {
  // 1 criar um vetor com 1 pos a menos
  pessoa *novaListaSequencial = new pessoa[*tamanhoDaLista - 1];
  // 2 colocar os valores antigos a partir do segundo (i-1, pra receber 0 no 1 antigo)
  for(int i = 1; i < *tamanhoDaLista; i++) {
    novaListaSequencial[i-1].nome = ponteiroSequencial[i].nome;
    novaListaSequencial[i-1].rg = ponteiroSequencial[i].rg;
  }
  // atualizar o ponteiro para a lista nova
  ponteiroSequencial = novaListaSequencial;
  /// reduzir o tamanho da lista
  *tamanhoDaLista = *tamanhoDaLista - 1;
}

void remove_fim_sequencial(pessoa *&ponteiroSequencial, int *tamanhoDaLista) {
  // 1 criar um vetor com 1 pos a menos
  pessoa *novaListaSequencial = new pessoa[*tamanhoDaLista - 1];
  // 2 colocar os valores antigos menos o ultimo
  for(int i = 0; i < *tamanhoDaLista-1; i++) {
    novaListaSequencial[i].nome = ponteiroSequencial[i].nome;
    novaListaSequencial[i].rg = ponteiroSequencial[i].rg;
  }
  // atualizar o ponteiro para a lista nova
  ponteiroSequencial = novaListaSequencial;
  /// reduzir o tamanho da lista
  *tamanhoDaLista = *tamanhoDaLista - 1;
}
// removendo do meio
void remove_pos_sequencial(pessoa *&ponteiroSequencial, int *tamanhoDaLista, int posicao) {
  // 1 criar vetor menor
  pessoa *novaListaSequencial = new pessoa[*tamanhoDaLista - 1];
  // 2 passar os valores antigos, pulando pos X
  for(int i = 0; i < posicao; i++) {
    novaListaSequencial[i].nome = ponteiroSequencial[i].nome;
    novaListaSequencial[i].rg = ponteiroSequencial[i].rg;
  }
  for(int j = posicao+1; j < *tamanhoDaLista; j++) {
    novaListaSequencial[j-1].nome = ponteiroSequencial[j].nome;
    novaListaSequencial[j-1].rg = ponteiroSequencial[j].rg;
  }
  // atualizar o ponteiro para a lista nova
  ponteiroSequencial = novaListaSequencial;
  /// reduzir o tamanho da lista
  *tamanhoDaLista = *tamanhoDaLista - 1;
}

string retorna_nome_sequencial(pessoa *&ponteiroSequencial, int *tamanhoDaLista, int rg) {
  string nome = "Nao encontrado";
  // busca pelo nome com o RG digitado
  for(int i = 0; i < *tamanhoDaLista; i++) {
    if(ponteiroSequencial[i].rg == rg) {
      nome = ponteiroSequencial[i].nome;
    }
  }
  return nome;
}

void limpa_tela() {
  system("clear");
}

int main () {
  // vars
  int funcaoDesejada = 1;
  // ponteiro para lista sequencial
  pessoa *ponteiroSequencial;
  // tamanho da lista, encadeada diz onde tá o final, sequencial n
  int tamanhoDaLista = 2; // variavel pra cuidar do tamanho do vetor, sempre 1 a +
  // lista de exemplo
  pessoa *exemploListaSequencial = new pessoa[tamanhoDaLista];
  exemploListaSequencial[0].nome = "Micha";
  exemploListaSequencial[0].rg = 124578;
  exemploListaSequencial[1].nome = "Luci";
  exemploListaSequencial[1].rg = 369;
  // faz o ponteiro pricipal apontar para o novo vetor
  ponteiroSequencial = exemploListaSequencial;

  while(funcaoDesejada < 9 && funcaoDesejada > 0) {
    printf("\nOperacoes\n");
    printf("1- Insercao de um NODE no inicio da lista\n");
    printf("2- Insercao de um NODe no fim da lista\n");
    printf("3- Insercao de um NODE na posicao N\n");
    printf("4- Retirar um NODE do inicio da lista\n");
    printf("5- Retirar um NODE no fim da lista\n");
    printf("6- Retirar um NODe na posicao N\n");
    printf("7- Procurar um NODE com o campo RG\n");
    printf("8- Imprimir lista\n");
    printf("9- Sair do sistema\n");
    printf("\nEscolha um numero e pressione ENTER: \n");
    //
    cin >> funcaoDesejada;

    limpa_tela();

    // vars para operações por parametro
    string nome;
    int rg;
    int posicao;

    switch (funcaoDesejada) {
      case 1:
        printf("\nFuncao escolhida: 1- Insercao de um NODE no inicio da lista\n");
        cout << "Digite um Nome:";
        cin >> nome;
        cout << "Digite um RG:";
        cin >> rg;
        limpa_tela();
        // ponteiroSequencial= ponteiro principal pra lista
        // tamanho da lista em forma de referencia pra ser retornado e atualizado
        adc_comeco_sequencial(ponteiroSequencial, &tamanhoDaLista, nome, rg);
        break;
      case 2:
        printf("\nFuncao escolhida: 2- Insercao de um NODe no fim da lista\n");
        cout << "Digite um Nome:";
        cin >> nome;
        cout << "Digite um RG:";
        cin >> rg;
        limpa_tela();
        // se a lista for vazia, já cria uma na func criar no inicio
        if(tamanhoDaLista == 0) adc_comeco_sequencial(ponteiroSequencial, &tamanhoDaLista, nome, rg);
        else adc_fim_sequencial(ponteiroSequencial, &tamanhoDaLista, nome, rg);
        break;
      case 3:
        printf("\nFuncao escolhida: 3- Insercao de um NODE na posicao N\n");
        cout << "Digite uma Posicao:";
        cin >> posicao;
        cout << "Digite um Nome:";
        cin >> nome;
        cout << "Digite um RG:";
        cin >> rg;
        limpa_tela();
        // se tiver tentando adicionar no começo
        if(posicao == 0) adc_comeco_sequencial(ponteiroSequencial, &tamanhoDaLista, nome, rg);
        // se tiver adicionando no final
        else if(posicao == tamanhoDaLista) adc_fim_sequencial(ponteiroSequencial, &tamanhoDaLista, nome, rg);
        // adiciona numa posição especifica
        else adc_pos_sequencial(ponteiroSequencial, &tamanhoDaLista, nome, rg, posicao);
        break;
      case 4:
        printf("\nFuncao escolhida: 4- Retirar um NODE do inicio da lista\n");
        if(tamanhoDaLista == 0) cout << "\nLISTA VAZIA\n";
        else remove_inicio_sequencial(ponteiroSequencial, &tamanhoDaLista);
        break;
      case 5:
        printf("\nFuncao escolhida: 5- Retirar um NODE no fim da lista\n");
        if(tamanhoDaLista == 0) cout << "\nLISTA VAZIA\n";
        else remove_fim_sequencial(ponteiroSequencial, &tamanhoDaLista);
        break;
      case 6:
        printf("\nFuncao escolhida: 6- Retirar um NODe na posicao N\n");
        if(tamanhoDaLista == 0) cout << "\nLISTA VAZIA\n";
        else {
          cout << "Digite uma Posicao:";
          cin >> posicao;
          limpa_tela();
          if(posicao == 0) remove_inicio_sequencial(ponteiroSequencial, &tamanhoDaLista);
          else if(posicao == tamanhoDaLista-1) remove_fim_sequencial(ponteiroSequencial, &tamanhoDaLista);
          else remove_pos_sequencial(ponteiroSequencial, &tamanhoDaLista, posicao);
        }
        break;
      case 7:
        printf("\nFuncao escolhida: 7- Procurar um NODE com o campo RG\n");
        cout << "Digite um RG:";
        cin >> rg;

        cout << "\n--- Pesquisa RG ---\n";
        cout << "Nome: " << retorna_nome_sequencial(ponteiroSequencial, &tamanhoDaLista, rg);
        cout << "\n-------------------\n";
        break;
      case 8:
        printf("\nFuncao escolhida: 8- Imprimir lista\n");
        // imprime lista completa
        imprime_sequencial(ponteiroSequencial, tamanhoDaLista);
        break;
      case 9:
        printf("\nFuncao escolhida: 9- Sair do sistema\n");
        break;
    }
  }
  return 0;
}
