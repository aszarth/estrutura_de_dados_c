// busca pesquisa sequencial em vetor
#include <iostream>
#include <new>
#include <string>
#include <stdlib.h>
#include <stdio.h> // usar o printf do C normal

#define MAX_TAM 10

using namespace std;

/* resumo rapido sobre ponteiros:
  qnd vc passa por parametro &, vc faz uma passagem por referencia, por exemplo:
  chamar= func(&var)
  recebendo numa função= func(*var)
  vc tá passando o endereço na memoria, o que quer dizer que quando alterar
  vai alterar a variavel lá onde ela está, não só localmente no escopo da função, como passado por parametro normalmente
*/

int busca_simples(int vetor[MAX_TAM], int valorProcurado, int *posicaoEncontrada) {
  int i; // auxiliar contador
  bool valorFoiEncontrado;

  for(i = 0; i < MAX_TAM; i++) {
    if(vetor[i] == valorProcurado) {
      valorFoiEncontrado = true;
      *posicaoEncontrada = i;
    }
  }

  if(valorFoiEncontrado) return 1;
  else return -1;
}

void imprime_vetor(int vetor[MAX_TAM]) {
  int i;
  for(i = 0; i < MAX_TAM-1; i++) {
    cout << vetor[i] << " - ";
  }
}




int main() {
  int vetor[MAX_TAM] = {1, 23, 44, 56, 63, 72, 84, 90, 98};
  int valorProcurado;
  int posicao, posicaoEncontrada;

  imprime_vetor(vetor);

  printf("Qual numero deseja encontrar?");
  scanf("%d", &valorProcurado);

  if(busca_simples(vetor, valorProcurado, &posicaoEncontrada) == 1) {
    cout << "O valor foi encontrado na posicao: " << posicaoEncontrada;
  } else {
    cout << "Valor nao encontrado";
  }


  return 0;
}
