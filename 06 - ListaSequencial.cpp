#include <stdio.h>
#include <stdlib.h>

#include <new> // pra alocar vetor
/*
em C
int *vetorLidoNaHora;
// passa o esppaço da memoria que foi criado para o vetor
vetorLidoNaHora = aloca_vetor(tamanhoDaLista);

é igual, em c++:
int *vetorLidoNaHora = new int[tamanhoDaLista];

int* aloca_vetor(int tam) {
  // cria um ponteiro auxiliar
  int *v;
  // aloca memoria para o novo vetor
  v = (int *) malloc(tam * sizeof(int)); // (int *) cast pra forçar o ponteiro
  return v;
}
*/

// tamanhoDaListaSequencial = sizeOf de outras linguagens
void imprime_sequencial(int *vetor, int tamanhoDaListaSequencial) {
  printf("\nImprimindo vetor:");
  // exibindo valores
  for(int i = 0; i < tamanhoDaListaSequencial; i++) {
    printf("\nValor[%d] = %d", i, vetor[i]);
  }
}

int main() {
  int tamanhoDaLista;
  // pedindo tamanho do vetor
  printf("\nDigite o tamanho do vetor:\n");
  scanf("%d", &tamanhoDaLista);
  // ponteiro para novo vetor
  // passa o esppaço da memoria que foi criado para o vetor
  int *vetorLidoNaHora = new int[tamanhoDaLista];
  //
  printf("\nDigite os valores do vetor:\n");
  for(int i = 0; i < tamanhoDaLista; i++) {
    printf("\npos[%d]:\n", i);
    scanf("%d", &vetorLidoNaHora[i]);
  }
  imprime_sequencial(vetorLidoNaHora, tamanhoDaLista);
  return 0;
}
