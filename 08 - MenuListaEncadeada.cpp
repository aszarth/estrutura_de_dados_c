#include <iostream> // cout
#include <new> // malok
#include <string> // string em vez de vetor de caracter
#include <stdlib.h> // pra parar o sistema e tal

// cout
using namespace std;

struct pessoa {
    string nome;
    int rg;
    struct pessoa *proximo;
};

void limpa_tela() {
  system("clear");
}

int retorna_tamanho(pessoa *ponteiroEncadeada) {
  // se a lista estiver vazia
  if(ponteiroEncadeada->nome == "") return 0;
  //
  int tamanho = 0;
  // ponteiro cursos auxiliar
  pessoa *p = ponteiroEncadeada;

  while(p != NULL) {
    // atualiza o cursor
    p = p->proximo;
    // aumenta o contador
    tamanho++;
  }
  return tamanho;
}

void imprime_encadeada(pessoa *ponteiroEncadeada) {
  if(retorna_tamanho(ponteiroEncadeada) == 0) {
    cout << "\nLISTA VAZIA\n";
  }
  else {
    // ponteiro aux
    pessoa *p = ponteiroEncadeada;

    while(p != NULL) {
      // imprime a lista
      cout << p->nome << ", " << p->rg << "\n";

      // atualiza o cursor
      p = p->proximo;
    }
  }
}

void adc_inicio_encadeada(pessoa *&ponteiroEncadeada, string nome, int rg) {
  // cria uma nova estrutura
  pessoa *novoValor = new pessoa;
  novoValor->nome = nome;
  novoValor->rg = rg;

  // se a lista está vazia
  if(ponteiroEncadeada->nome == "") novoValor->proximo = NULL;
  else novoValor->proximo = ponteiroEncadeada;

  // redireciona o ponteiro
  ponteiroEncadeada = novoValor;
}

void adc_fim_encadeada(pessoa *&ponteiroEncadeada, string nome, int rg) {
  // 1 cria um novo registro com campo proximo NULL
  pessoa *novoValor = new pessoa;
  novoValor->nome = nome;
  novoValor->rg = rg;
  novoValor->proximo = NULL;
  // 2 criar cursor, ponteiro aux
  pessoa *p = ponteiroEncadeada;
  while(p != NULL) {
    // 3 chegar no final da lista, fazer com que o ultimo elemento aponte para o novo elemento
    if(p->proximo == NULL) {
      p->proximo = novoValor;
      return; // para, encontrou, não precisa mais atualizar o cursor, sai do while, termina
    }
    // atualiza o ponteiro
    p = p->proximo;
  }
}

void adc_meio_encadeada(pessoa *&ponteiroEncadeada, string nome, int rg, int posicao) {
  // 1 cria novo registro
  pessoa *novoValor = new pessoa;
  novoValor->nome = nome;
  novoValor->rg = rg;
  novoValor->proximo = NULL;
  // 2 navegar ate 1 pos antes da desejada
  // 3 fazer com que o proximo dessa pos aponte para o novo
  pessoa *p = ponteiroEncadeada; // ponteiro aux
  // contador de posicoes
  int i = 0;
  // até 1 antes
  while(i <= posicao) {
    if(i == posicao-1) {
      // auxiliar do valor
      pessoa *aux = new pessoa;
      // armazena o proximo valor
      aux->proximo = p->proximo;
      // coloca o novo valor como o proximo dele
      p->proximo = novoValor;
      // faz com que o novo valor aponte para o proximo
      novoValor->proximo = aux->proximo;
      // liberar o auxiliar
      free(aux);
    }
    // atualiza ponteiro aux
    p = p->proximo;
    // incrementa contador
    i++;
  }
}

void remove_inicio_encadeada(pessoa *&ponteiroEncadeada) {
  // se só existir 1 membro
  if(ponteiroEncadeada->proximo == NULL) {
    // cria novo registro VAZIO
    pessoa *novoValor = new pessoa;
    novoValor->nome = "";
    novoValor->rg = 0;
    novoValor->proximo = NULL;

    ponteiroEncadeada = novoValor;
  }
  else {
    // faz o ponteiro principal apontar para o proximo valor
    ponteiroEncadeada = ponteiroEncadeada->proximo;
  }
}

// andar pela lista, chegar no penultimo, fazendo o proximo dele apontar para NULL
void remove_fim_encadeada(pessoa *&ponteiroEncadeada) {
  // auxiliares
  pessoa *p = new pessoa;
  pessoa *aux = new pessoa;
  //
  p = ponteiroEncadeada;
  // navegar na lista
  while(p->proximo != NULL) {
    // auxiliar se transforma sempre em um elemento antes do cursor
    aux = p;
    // passa o cursor para o proximo elemento
    p = p->proximo;
  }
  // muda o final da lista para o penultimo elemento
  aux->proximo = NULL;
}

void remove_pos_encadeada(pessoa *&ponteiroEncadeada, int posicao) {
  // auxiliares
  pessoa *p = ponteiroEncadeada; // sempre que tem que navegar por uma lista encadeada, tem que ter um p aux
  int i = 0; // contador de posicoes

  while(i <= posicao) {
    if(i == posicao-1) {
      // cria um auxiliar pessoa
      pessoa *aux = new pessoa;

      // auxiliar recebe o elemento que sera eliminado
      aux = p->proximo;

      // faz com que o proximo pule um elemento
      p->proximo = aux->proximo;

      // libera a memoria, nao vai estar mais utilizando
      free(aux);
    }

    // passa o cursor para o proximo elemento
    p = p->proximo;
    // registra uma movimentação
    i++;
  }
}

string retorna_nome_encadeada(pessoa *&ponteiroEncadeada, int rg) {
  // nome a ser retornado
  string nome = "nao encontrado";
  // aux
  pessoa *p = ponteiroEncadeada;
  // percorrer lista
  while(p != NULL) {
    // encontra o RG
    if(p->rg == rg) {
      nome = p->nome;
      return nome;
    }
    p = p->proximo;
  }
  return nome;
}

int main () {
  // vars
  int funcaoDesejada = 1;

  // cria o inicio da lista encadeada, zerada pra fazer as verificações
  pessoa *ponteiroEncadeada = new pessoa;
  ponteiroEncadeada->nome = "";
  ponteiroEncadeada->rg = 0;
  ponteiroEncadeada->proximo = NULL;

  // cria o primeiro valor
  pessoa *novoPrimeiroValor = new pessoa;
  novoPrimeiroValor->nome = "Micha";
  novoPrimeiroValor->rg = 124578;
  novoPrimeiroValor->proximo = NULL;

  // transforma o ponteiro do inicio da lista no novo valor
  ponteiroEncadeada = novoPrimeiroValor;

  // adiciona valor 2
  pessoa *novoSegundoValor = new pessoa;
  novoSegundoValor->nome = "Luci";
  novoSegundoValor->rg = 369;
  novoSegundoValor->proximo = NULL;

  // encadeamento
  novoPrimeiroValor->proximo = novoSegundoValor;

  //
  while(funcaoDesejada < 9 && funcaoDesejada > 0) {
    // mostra menu
    cout << "\nTamanho Atual: " << retorna_tamanho(ponteiroEncadeada) << "\n";
    // imprime a lista
    imprime_encadeada(ponteiroEncadeada);
    //
    cout << "\nOperacoes\n";
    cout << "1- Insercao de um NODE no inicio da lista\n";
    cout << "2- Insercao de um NODe no fim da lista\n";
    cout << "3- Insercao de um NODE na posicao N\n";
    cout << "4- Retirar um NODE do inicio da lista\n";
    cout << "5- Retirar um NODE no fim da lista\n";
    cout << "6- Retirar um NODe na posicao N\n";
    cout << "7- Procurar um NODE com o campo RG\n";
    cout << "8- Imprimir lista\n";
    cout << "9- Sair do sistema\n";
    cout << "\nEscolha um numero e pressione ENTER: \n";
    // usuario escolhe opcao
    cin >> funcaoDesejada;
    limpa_tela();

    // temp vars
    string nome;
    int rg;
    int posicao;

    // chama funcao desejada
    switch (funcaoDesejada) {
      case 1:
        cout << "\nFuncao escolhida: 1- Insercao de um NODE no inicio da lista\n";
        cout << "Digite o nome: ";
        cin >> nome;
        cout << "Digite o RG: ";
        cin >> rg;

        adc_inicio_encadeada(ponteiroEncadeada, nome, rg);
        break;
      case 2:
        cout << "\nFuncao escolhida: 2- Insercao de um NODe no fim da lista\n";
        cout << "Digite o nome: ";
        cin >> nome;
        cout << "Digite o RG: ";
        cin >> rg;
        // se tentar adicionar ao fim da lista, mas ela tiver vazia, ja joga pro inicio q lá trata
        if(retorna_tamanho(ponteiroEncadeada) == 0) adc_inicio_encadeada(ponteiroEncadeada, nome, rg);
        else adc_fim_encadeada(ponteiroEncadeada, nome, rg);
        break;
      case 3:
        cout << "\nFuncao escolhida: 3- Insercao de um NODE na posicao N\n";
        cout << "Digite a posicao: ";
        cin >> posicao;
        cout << "Digite o nome: ";
        cin >> nome;
        cout << "Digite o RG: ";
        cin >> rg;
        if(posicao == 0) adc_inicio_encadeada(ponteiroEncadeada, nome, rg);
        // -1 pq começa em 0
        else if(posicao == retorna_tamanho(ponteiroEncadeada)-1) adc_fim_encadeada(ponteiroEncadeada, nome, rg);
        else adc_meio_encadeada(ponteiroEncadeada, nome, rg, posicao);
        break;
      case 4:
        cout << "\nFuncao escolhida: 4- Retirar um NODE do inicio da lista\n";
        remove_inicio_encadeada(ponteiroEncadeada);
        break;
      case 5:
        cout << "\nFuncao escolhida: 5- Retirar um NODE no fim da lista\n";
        if(retorna_tamanho(ponteiroEncadeada) == 1) remove_inicio_encadeada(ponteiroEncadeada);
        else remove_fim_encadeada(ponteiroEncadeada);
        break;
      case 6:
        cout << "\nFuncao escolhida: 6- Retirar um NODe na posicao N\n";

        cout << "\nDigite a posicao: ";
        cin >> posicao;

        if(posicao == 0) remove_inicio_encadeada(ponteiroEncadeada);
        else if(posicao == retorna_tamanho(ponteiroEncadeada) - 1) remove_fim_encadeada(ponteiroEncadeada);
        else remove_pos_encadeada(ponteiroEncadeada, posicao);
        break;
      case 7:
        cout << "\nFuncao escolhida: 7- Procurar um NODE com o campo RG\n";
        // rg buscado pelo usuario
        cout << "Digite o RG: ";
        cin >> rg;

        cout << "\nO nome do RG " << rg << " e: " << retorna_nome_encadeada(ponteiroEncadeada, rg) << "\n";
        break;
      case 8:
        cout << "\nFuncao escolhida: 8- Imprimir lista\n";
        break;
      case 9:
        cout << "\nFuncao escolhida: 9- Sair do sistema\n";
        break;
    }
  }
  return 0;
}
