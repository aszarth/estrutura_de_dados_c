/*
  First in First Out

  Como se fosse uma fila no banco

  enquanto na pilha tem que ir tirando todos da frente até chegar no objetivo
  a fila é o primeiro a entrar é o primeiro a sair, como uma ordem de chegada
*/

#include <iostream>
#include <stdlib.h>
#include <string>

#define TAM 10

using namespace std;

void imprime_vetor(int vetor[TAM], int tamanho) {
  int cont;

  cout << "\n";

  for(cont = 0; cont < TAM; cont++) {
    cout << vetor[cont] << " - ";
  }
}

void fila_construtor(int *frente, int *tras) {
  *frente = 0;
  *tras = -1;
}

bool fila_vazia(int frente, int tras) {
  if(frente > tras) return true;
  else return false;
}

bool fila_cheia(int *tras) {
  if(*tras == TAM - 1) return true;
  else return false;
}

void fila_enfileirar(int fila[TAM], int valor, int *tras) {
  if(fila_cheia(tras)) {
    printf("fila cheia");
  }
  else {
    *tras = *tras + 1; // ponteiro não funciona com ++ no C
    fila[*tras] = valor;
  }
}

void fila_desenfilerar(int fila[TAM], int *frente, int tras) {
  if(fila_vazia(*frente, tras)) {
    printf("fila vazia");
  }
  else {
    printf("O valor %i foi removido\n", fila[*frente]);
    fila[*frente] = 0;
    *frente = *frente + 1;
  }
}

int fila_tamanho(int tras, int frente) {
  return (tras - frente) + 1;
}

int main() {
  //
  int fila[TAM] = {0,0,0,0,0,0,0,0,0,0};
  int frente,tras;
  int valor;

  fila_construtor(&frente, &tras);

  fila_enfileirar(fila, 1, &tras);
  fila_enfileirar(fila, 2, &tras);
  fila_enfileirar(fila, 3, &tras);
  fila_enfileirar(fila, 4, &tras);
  fila_enfileirar(fila, 5, &tras);
  fila_enfileirar(fila, 6, &tras);

  fila_desenfilerar(fila, &frente, tras);
  fila_desenfilerar(fila, &frente, tras);

  fila_enfileirar(fila, 7, &tras);
  fila_enfileirar(fila, 8, &tras);
  fila_enfileirar(fila, 9, &tras);
  fila_enfileirar(fila, 10, &tras);
  fila_enfileirar(fila, 11, &tras);

  imprime_vetor(fila, fila_tamanho(tras, frente) );

  return 0;
}
