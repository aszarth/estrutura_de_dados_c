/*
  buble_sort:
  vai conferindo os pares e vai trocando
  pega o primeiro compara com o segundo, vê quem é maior e troca se necessario

  insert_sort:
  le um valor, joga todos os outros maiores pra frente
  e insere o valor na posição certa

  selection_sort:
  vai comparando de dois em dois os elementos próximos
  acha o menor de todos vai jogando pra tras
  (muito bom para vetores pequenos)

  quicky_sort:
  utilizado qnd n sabe como a lista vai estar (ordenada, +/-, invertida)
  organizia os elementos da lista em pequenos numeros menores

  shell_sort:
  funciona muito bem pra listas muito grandes

  merge_sort:
  faz pequenas ordenações e agrupa os grupos de ordenação
  dividir para conquistar
*/

#include <iostream>
#include <stdlib.h>
#include <string>

#define TAM 10

using namespace std;

void imprime_vetor(int vetor[]) {
  cout << "\n";
  for(int i = 0; i < TAM; i++) {
    printf(" [%i] ", vetor[i]);
  }
}

void buble_sort(int vetor[TAM]) {
  int x, y, aux;
  // valor da esquerda a ser analisado
  for(x = 0; x < TAM; x++) {
  // valor da direita a ser analisado
  for(y = x+1; y < TAM; y++) {
    imprime_vetor(vetor);
    // confere se precisa fazer a troca
    if(vetor[x] > vetor[y]) {
      aux = vetor[x];
      vetor[x] = vetor[y];
      vetor[y] = aux;
    }
  }
  }
}

void insert_sort(int vetor[TAM]) {
  int i, j, atual;
  for(i = 1; i < TAM; i++) {
    // elemento atual em analise
    atual = vetor[i];
    // elemento anterior analisado
    j = i - 1;
    // arrastar geral pra frente e deixar um espaço vago
    while(j >= 0 && atual < vetor[j]) {

      // posiciona os elementos uma posição para frente
      vetor[j+1] = vetor[j];
      j = j - 1;
    }
    // agora que o espaço está aberto colocamos o menor numero na posição correta
    vetor[j+1] = atual;
    // mostra a lista atualizada
    imprime_vetor(vetor);
  }
}

void selection_sort(int vetor[TAM]) {
  int posMenorValorEncontrado; // posicaoDoMenorValorEncontradoAteOMomento
  int aux, i, j;
  for(i = 0; i < TAM; i++) {

    // recebe a poisção inicial para o menor valor
    posMenorValorEncontrado = i;

    // cursos passeando, analisa os elementos na frente (apos o amarelo)
    for(j = i + 1; j < TAM; j++) {
      // encontre um valor menor na frente dos analisados
      if(vetor[j] < vetor[posMenorValorEncontrado]) {
        posMenorValorEncontrado = j;
      }
    }

    // houve mudança, troca os valores
    if(posMenorValorEncontrado != i) {
      aux = vetor[i];
      vetor[i] = vetor[posMenorValorEncontrado];
      vetor[posMenorValorEncontrado] = aux;
    }
    imprime_vetor(vetor);
  }
}

void quicky_sort(int vetor[TAM], int inicio, int fim) {
  int pivo, meio; // pivo = valor meio, meio= pos meio
  int esq, dir; // limite da esquerda e da direita
  int aux; //

  // limite  da esquerda e direita da região analisada
  esq = inicio;
  dir = fim;

  while(dir > esq) {
    // ajustando auxiliares do centro
    meio = (int) (esq + dir) / 2; // int pra não ter numero quebrado (cast)
    pivo = vetor[meio];

    while(vetor[esq] < pivo) {
      esq = esq + 1;
    }
    while(vetor[dir] > pivo) {
      dir = dir - 1;
    }
    if(esq <= dir) { // limite da esquerda e da direita, se cruzaram
      // realizar uma troca
      aux = vetor[esq];
      vetor[esq] = vetor[dir];
      vetor[dir] = aux;

      // faz os limites laterais caminharem para o centro
      esq = esq + 1;
      dir = dir - 1;
    }
    //
    imprime_vetor(vetor);
  }
  // recursão= chamar a função dentro da função
  // para continuar ordenando
  if(inicio < dir) {
    quicky_sort(vetor, inicio, dir);
  }
  if(esq < fim) {
    quicky_sort(vetor, esq, fim);
  }
}

void shell_sort(int vetor[TAM]) {
  int i, j, atual;
  int h = 1; // pulo de qnt tem q comparar

  // em qnts sera o pulo entre analises
  while(h < TAM) {
    h = 3*h+1; // +1 por causa do vetor, tem 10=9
  }

  while(h > 1) {
    // reduz a distancia entre as analises
    h = h / 3;


    // insertion sort, mas em vez de troca lado a lado, troca em distancia H
    for(i = h; i < TAM; i++) {
      // elemento atual em analise
      atual = vetor[i];
      // elemento anterior analisado
      j = i - h;
      // arrastar geral pra frente e deixar um espaço vago
      while(j >= 0 && atual < vetor[j]) {

        // posiciona os elementos uma posição para frente
        vetor[j+h] = vetor[j];
        j = j - h;
      }
      // agora que o espaço está aberto colocamos o menor numero na posição correta
      vetor[j+h] = atual;
      // mostra a lista atualizada
      imprime_vetor(vetor);
    }

  }

}


    // une os dois subarrays que foram criados ao dividir o vetor principal
void merge(int vetor[TAM], int indiceEsquerdo, int meio, int indiceDireito) {
  int n1 = meio - indiceEsquerdo + 1; // tamanho do primeiro vetor auxiliar
  int n2 = indiceDireito - meio; // tamanho do segundo vetor auxiliar
  // cria 2 arrays temporarios
  int vetorEsquerdo[n1];
  int vetorDireito[n2];

  // passa os elementos do vetor principal para o primeiro vetor auxiliar (esquerda)
  int i, j, k; // auxiliares contadores
  for(i = 0; i < n1; i++) {
    vetorEsquerdo[i] = vetor[indiceEsquerdo + i];
  }
  // passa os elementos do vetor principal para o segundo vetor auxiliar (direita)
  for(j = 0; j < n2; j++) {
    vetorDireito[j] = vetor[meio + 1 + j];
  }
  // reseta as variaveis
  i = 0;
  j = 0;
  k = indiceEsquerdo;
  //
  while(i < n1 && j < n2)
  {
      // caso o valor na esquerda seja menor
      if(vetorEsquerdo[i] <= vetorDireito[j]) {
        vetor[k] = vetorEsquerdo[i];
        i++;
      }
      else {
        vetor[k] = vetorDireito[j];
        j++;
      }
      k++;
  }
  // se faltarem alguns elementos do array ESQUERDO, passar eles para o vetor principal
  while(i < n1) {
    vetor[k] = vetorEsquerdo[i];
    i++;
    k++;
  }
  // se faltarem alguns elementos do array DIREITO, passar eles para o vetor principal
  while(j < n2) {
    vetor[k] = vetorDireito[i];
    j++;
    k++;
  }
}
void merge_sort(int vetor[TAM], int indiceEsquerdo, int indiceDireito) {
  // ainda precisa fazer ordenações
  if(indiceEsquerdo < indiceDireito) {
    // encontra pos meio
    int meio = indiceEsquerdo + (indiceDireito-indiceEsquerdo)/2;

    // da metade pra tras
    merge_sort(vetor, indiceEsquerdo, meio);
    // da metade pra frente
    merge_sort(vetor, meio+1, indiceDireito);

    // visualizar o processo
    imprime_vetor(vetor);

    // pilha, last in first out, dps de toda a recursao e acabar a condicao acima, faz a ordenação
    merge(vetor, indiceEsquerdo, meio, indiceDireito);
  }
}

int main() {
  int vetor[TAM] = {10,9,8,7,6,5,4,3,2,1};
  imprime_vetor(vetor);

  merge_sort(vetor, 0, TAM - 1);

  //shell_sort(vetor);
  //quicky_sort(vetor, 0, TAM);
  //selection_sort(vetor);
  //insert_sort(vetor);
  //buble_sort(vetor);
  // no c toda vez que vc manda um vetor por parametro, automaticamente ele é passado por referencia
  // quando imprimir o ultimo vetor ali ele já vai estar alterado
  printf("\nFinish at:");
  imprime_vetor(vetor);
  return 0;
}
